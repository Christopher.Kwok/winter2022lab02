public class MethodsTest{
	
	public static void main(String[] args){
		int x = 10;
		
		/*methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x + 50);
		
		methodTwoInputNoReturn(50, 1.5);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		System.out.println(sumSquareRoot(6,3));
		String s1 = "hello";
		String s2 = "goodbye";
		
		System.out.println(s1.length());
		System.out.println(s2.length());*/
		
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn(){
		
		int x = 50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int num){
		
		System.out.println("Inside the method one input no return");
		
	}
	public static void methodTwoInputNoReturn(int numInt, double numDouble){
		
		System.out.println("Inside the method two input no return");
	}
	public static int methodNoInputReturnInt(){
		
		return 6;
		
	}
	public static double sumSquareRoot(int x, int y){
		
		int z = x + y;
		return Math.sqrt(z);
	}
}
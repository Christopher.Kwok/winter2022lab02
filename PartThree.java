import java.util.Scanner;

public class PartThree{
	
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Please input a length for your square");
		int squareLength = reader.nextInt();
		
		System.out.println("Please input a length for your rectangle followed by a width");
		int rectangleLength = reader.nextInt();
		int rectangleWidth = reader.nextInt();
		
		int squareArea = AreaComputations.areaSquare(squareLength);
		
		AreaComputations ac = new AreaComputations();
		int rectangleArea = ac.areaRectangle(rectangleLength, rectangleWidth);
		
		//I use un^2 to represent the area as I do not know the unit that they wish to calculate.
		System.out.println("Your square's area is " + squareArea + " un^2 and your rectangle's area is " + rectangleArea + " un^2!");
	}
	
}